import "./App.css";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plug
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import { Modal, Button, Form } from "react-bootstrap";
import { useState } from "react";

function App() {
  const [show, setShow] = useState(false);
  const [showCreatedModal, setshowCreatedModal] = useState(false);
  const [eventAPI, seteventAPI] = useState([
    { title: "event 1", date: "2022-05-13" },
    { title: "event 2", date: "2022-05-15" },
    {
      title: "daily meeting",
      date: "2022-05-17",
      color: "red",
      textColor: "white",
      borderColor: "red",
      allDay: true,
    },
    {
      id: 2,
      title: "BCH237",
      start: "2022-05-20T10:30:00",
      end: "2022-05-21T11:30:00",
      extendedProps: {
        department: "BioChemistry",
      },
      description: "Lecture",
    },
  ]);
  const [SelectedEvent, setSelectedEvent] = useState({
    title: "",
    date: "",
  });
  const [SelectedCreatedEvent, setSelectedCreatedEvent] = useState({
    title: "",
    date: "",
  });

  // handleDateClick = (arg) => { // bind with an arrow function
  //   alert(arg.dateStr)
  // }
  function handleDateClick(arg) {
    // console.log('arg', arg)
    setSelectedCreatedEvent({ date: arg.dateStr });
    setshowCreatedModal(true);

    // modal bootstrap (date(arg.dateStr) ,title( input (onChange)))
    // axios .post data
    // send
    console.log("checking the handle date click", arg.dateStr);
  }
  function handleSelectClick(arg) {
    console.log("checking the handleSelectClick date click", arg);
  }
  function handleEventClick(arg) {
    // n7el el modal
    // n3abi les champs mtaai ali jayin mel arg fel popup
    // console.log('checking the handleEventClick date click', arg)
    setSelectedEvent({ title: arg.event.title, date: arg.event.startStr });
    setShow(true);
  }
  function handleSubmit() {
    console.log("checking the new state", SelectedEvent);
  }
  function handleSubmitCreated() {
    seteventAPI([...eventAPI,SelectedCreatedEvent]);
    eventApi.push({
      title: SelectedCreatedEvent.title,
      date: SelectedCreatedEvent.date,
    });
    setshowCreatedModal(false);
    console.log("checking the new state", eventAPI);
  }
  // fetch api axios fetch
  // event eventApi = results from the api

  const eventApi = [
    { title: "event 1", date: "2022-05-13" },
    { title: "event 2", date: "2022-05-15" },
    {
      title: "daily meeting",
      date: "2022-05-17",
      color: "red",
      textColor: "white",
      borderColor: "red",
      allDay: true,
    },
    {
      id: 2,
      title: "BCH237",
      start: "2022-05-20T10:30:00",
      end: "2022-05-21T11:30:00",
      extendedProps: {
        department: "BioChemistry",
      },
      description: "Lecture",
    },
  ];
  const handleClose = () => {
    setShow(false);
  };
  const handleCloseCreatedModal = () => {
    console.log("hello close", SelectedCreatedEvent);
    console.log("our eventApi", eventApi);
    setshowCreatedModal(false);
  };
  return (
    <div className="App">
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Event Title</Form.Label>
              <Form.Control
                type="input"
                placeholder="name@example.com"
                value={SelectedEvent.title}
                autoFocus
                onChange={(e) =>
                  setSelectedEvent({ ...SelectedEvent, title: e.target.value })
                }
                // readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label> Event Date</Form.Label>
              <Form.Control
                type="date"
                autoFocus
                value={SelectedEvent.date}
                onChange={(e) =>
                  setSelectedEvent({ ...SelectedEvent, date: e.target.value })
                }
                // readOnly
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showCreatedModal} onHide={handleCloseCreatedModal}>
        <Modal.Header closeButton>Create an Event</Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Event Title</Form.Label>
              <Form.Control
                type="input"
                placeholder="name@example.com"
                autoFocus
                defaultValue={SelectedCreatedEvent.title}
                onChange={(e) =>
                  setSelectedCreatedEvent({
                    ...SelectedCreatedEvent,
                    title: e.target.value,
                  })
                }
                // readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label> Event Date</Form.Label>
              <Form.Control
                type="date"
                autoFocus
                defaultValue={SelectedCreatedEvent.date}
                onChange={(e) =>
                  setSelectedCreatedEvent({
                    ...SelectedCreatedEvent,
                    date: e.target.value,
                  })
                }
                // readOnly
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseCreatedModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmitCreated}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <FullCalendar
        plugins={[dayGridPlugin, interactionPlugin]}
        editable={true}
        initialView="dayGridMonth"
        weekends={true}
        select={handleSelectClick}
        eventClick={handleEventClick}
        events={eventAPI}
        // dateClick={(arg) => alert(arg.dateStr)}
        dateClick={handleDateClick}
      />
    </div>
  );
}

export default App;
